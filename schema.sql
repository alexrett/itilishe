create table application
(
    id                   int auto_increment
        primary key,
    title                int null,
    transport_cluster_id int null
);

create table companies
(
    id      int auto_increment
        primary key,
    title   varchar(255) not null,
    `1c_id` int          null,
    dsa_id  int          null,
    edit_id int          null
)
    comment 'юр лица компании';

create table company_departments
(
    id         int auto_increment
        primary key,
    title      varchar(255) not null,
    `1c_id`    int          null,
    edit_id    int          null,
    dsa_id     int          null,
    company_id int          null,
    constraint company_departments_companies_id_fk
        foreign key (company_id) references companies (id)
)
    comment 'Подразделения компании';

create table countries
(
    id    int auto_increment
        primary key,
    title varchar(255) not null
)
    comment 'Страны';

create table cities
(
    id         int auto_increment
        primary key,
    country_id int          null,
    title      varchar(255) null,
    constraint cities_countries_id_fk
        foreign key (country_id) references countries (id)
)
    comment 'Города';

create table company_offices
(
    id         int auto_increment
        primary key,
    company_id int          null,
    title      varchar(255) null,
    city_id    int          null,
    constraint company_offices_cities_id_fk
        foreign key (city_id) references cities (id)
)
    comment 'Офисы компании';

create table department_statistics
(
    id            int auto_increment
        primary key,
    department_id int          null,
    table_name    varchar(255) null,
    updated_at    datetime     null
)
    comment 'Сводные отчеты департаментов';

create table employer_position
(
    id    int auto_increment
        primary key,
    title varchar(255) null
)
    comment 'Должность сотрудника';

create table employer_profile
(
    id            int auto_increment
        primary key,
    email         varchar(255) not null,
    first_name    varchar(255) not null,
    middle_name   varchar(255) null,
    last_name     varchar(255) not null,
    full_name     varchar(255) not null,
    phone_mobile  varchar(14)  not null,
    phone_home    varchar(14)  null,
    phone_work    varchar(14)  null,
    mattermost_id varchar(255) null
)
    comment 'профиль сотрудника';

create table employer_types
(
    id    int auto_increment
        primary key,
    title varchar(255) null
)
    comment 'Тип трудоустройства сотрудника';

create table employers
(
    id            int auto_increment
        primary key,
    dsa_id        int null,
    edit_id       int null,
    `1c_id`       int null,
    profile_id    int null,
    type_id       int null,
    department_id int null,
    constraint employers___fk2
        foreign key (type_id) references employer_types (id),
    constraint employers___fk_3
        foreign key (department_id) references company_departments (id),
    constraint employers_employer_profile_id_fk
        foreign key (profile_id) references employer_profile (id)
)
    comment 'сотрудники компаний';

create table employer_salary
(
    id          int auto_increment
        primary key,
    employer_id int   null,
    gross       float not null,
    net         float not null,
    constraint employer_salary_employers_id_fk
        foreign key (employer_id) references employers (id)
)
    comment 'Зарплаты сотрудников';

create table employer_vacation
(
    id          int auto_increment
        primary key,
    employer_id int               null,
    start_at    date              null,
    end_at      date              null,
    id_approved tinyint default 0 null,
    constraint employer_vacation_employers_id_fk
        foreign key (employer_id) references employers (id)
);

create table google_status
(
    id    int auto_increment
        primary key,
    title varchar(255) not null
)
    comment 'Статус гугл аккаунтов';

create table google_accounts
(
    id               int auto_increment
        primary key,
    status_id        int          null,
    employer_id      int          null,
    size             float        null,
    last_backup_date date         null,
    backup_freq      tinyint      null,
    google_uid       varchar(255) null,
    constraint google_accounts_employers_id_fk
        foreign key (employer_id) references employers (id),
    constraint google_accounts_google_status_id_fk
        foreign key (status_id) references google_status (id)
);

create table infrastructure_cluster
(
    id    int auto_increment
        primary key,
    title varchar(255) null
);

create table application_infra_refs
(
    id                        int auto_increment
        primary key,
    application_id            int null,
    infrastructure_cluster_id int null,
    constraint application_infra_refs_application_id_fk
        foreign key (application_id) references application (id),
    constraint application_infra_refs_infrastructure_cluster_id_fk
        foreign key (infrastructure_cluster_id) references infrastructure_cluster (id)
);

create table application_infrastructure_relations
(
    id                        int auto_increment
        primary key,
    application_id            int null,
    infrastructure_cluster_id int null,
    constraint application_infra_relations_infrastructure_cluster_id_fk
        foreign key (infrastructure_cluster_id) references infrastructure_cluster (id),
    constraint application_infrastructure_relations_application_id_fk
        foreign key (application_id) references application (id)
);

create table internet_providers
(
    id        int auto_increment
        primary key,
    title     varchar(255) not null,
    office_id int          null,
    constraint internet_providers_company_offices_id_fk
        foreign key (office_id) references company_offices (id)
)
    comment 'Интернет провайдеры компании';

create table internet_provider_tariffs
(
    id                   int auto_increment
        primary key,
    amount_per_month     float null,
    internet_provider_id int   null,
    constraint internet_provider_tariffs_internet_providers_id_fk
        foreign key (internet_provider_id) references internet_providers (id)
);

create table racks
(
    id        int auto_increment
        primary key,
    title     varchar(255) not null,
    office_id int          null,
    constraint racks_company_offices_id_fk
        foreign key (office_id) references company_offices (id)
)
    comment 'Стойки';

create table sip_status
(
    id    int auto_increment
        primary key,
    title varchar(255) not null
);

create table sip_accounts
(
    id          int auto_increment
        primary key,
    sip_uid     varchar(16) null,
    employer_id int         null,
    status_id   int         null,
    office_id   int         null,
    constraint sip_accounts_company_offices_id_fk
        foreign key (office_id) references company_offices (id),
    constraint sip_accounts_employers_id_fk
        foreign key (employer_id) references employers (id),
    constraint sip_accounts_sip_status_id_fk
        foreign key (status_id) references sip_status (id)
)
    comment 'Аккаунты телефонии';

create table sip_tariffs
(
    id      int auto_increment
        primary key,
    context varchar(255) null,
    type    varchar(10)  null,
    amount  float        not null
)
    comment 'Тарифы на телефонию';

create table sip_statistic
(
    id            int auto_increment
        primary key,
    call_at       datetime      not null,
    duration      int default 0 null comment 'секунды',
    status        varchar(100)  null,
    record_file   varchar(255)  null,
    type          varchar(100)  null,
    from_number   varchar(16)   null,
    to_number     varchar(16)   null,
    sip_id        int           null,
    sip_tariff_id int           null,
    constraint sip_statistic_sip_accounts_id_fk
        foreign key (sip_id) references sip_accounts (id),
    constraint sip_statistic_sip_tariffs_id_fk
        foreign key (sip_tariff_id) references sip_tariffs (id)
);

create table technic_status
(
    id    int auto_increment
        primary key,
    title varchar(255) not null
)
    comment 'Статус техники';

create table technic_types
(
    id    int auto_increment
        primary key,
    title varchar(255) not null
)
    comment 'Тип техники';

create table technics
(
    id          int auto_increment
        primary key,
    title       varchar(255)         not null,
    sku         varchar(255)         null,
    amount      float                null,
    status_id   int                  null,
    buy_at      date                 null,
    deleted_at  datetime             null,
    type_id     int                  null,
    office_id   int                  null,
    is_in_stock tinyint(1) default 1 null,
    constraint technics_technic_status_id_fk
        foreign key (status_id) references technic_status (id),
    constraint technics_technic_types_id_fk
        foreign key (type_id) references technic_types (id)
)
    comment 'Техника компании';

create table employer_technics
(
    id          int auto_increment
        primary key,
    technic_id  int  null,
    employer_id int  null,
    issue_at    date not null,
    delivery_at date null,
    constraint employer_technics_employers_id_fk
        foreign key (employer_id) references employers (id),
    constraint employer_technics_technics_id_fk
        foreign key (technic_id) references technics (id)
)
    comment 'Техника на сотруднике';

create table hardware
(
    id         int auto_increment
        primary key,
    rack_id    int          null,
    title      varchar(255) null,
    technic_id int          null,
    constraint hardware_racks_id_fk
        foreign key (rack_id) references racks (id),
    constraint hardware_technics_id_fk
        foreign key (technic_id) references technics (id)
)
    comment 'Сервера и сетевое оборудование компании';

create table application_hardware
(
    id             int auto_increment
        primary key,
    application_id int null,
    hardware_id    int null,
    constraint application_hardware_application_id_fk
        foreign key (application_id) references application (id),
    constraint application_hardware_hardware_id_fk
        foreign key (hardware_id) references hardware (id)
);

create table infrastructure_cluster_hardwares
(
    id                        int auto_increment
        primary key,
    infrastructure_cluster_id int null,
    hardware_id               int null,
    constraint infrastructure_cluster_hardwares_hardware_id_fk
        foreign key (hardware_id) references hardware (id),
    constraint infrastructure_cluster_hardwares_infrastructure_cluster_id_fk
        foreign key (infrastructure_cluster_id) references infrastructure_cluster (id)
);

create table transport_clusters
(
    id    int auto_increment
        primary key,
    title varchar(255) null
)
    comment 'Кластера компании';

create table transport_cluster_hardwares
(
    id          int auto_increment
        primary key,
    cluster_id  int null,
    hardware_id int null,
    constraint cluster_hardwares_clusters_id_fk
        foreign key (cluster_id) references transport_clusters (id),
    constraint cluster_hardwares_hardware_id_fk
        foreign key (hardware_id) references hardware (id)
)
    comment 'Сервера для кластеров';

create table vms
(
    id         int auto_increment
        primary key,
    cluster_id int          null,
    title      varchar(255) null,
    status     varchar(50)  null,
    constraint vms_clusters_id_fk
        foreign key (cluster_id) references transport_clusters (id)
)
    comment 'Виртуальные машины';

create table application_vms
(
    id             int auto_increment
        primary key,
    vm_id          int null,
    application_id int null,
    constraint application_vms_application_id_fk
        foreign key (application_id) references application (id),
    constraint application_vms_vms_id_fk
        foreign key (vm_id) references vms (id)
);

create table infrastructure_cluster_vms
(
    id                        int auto_increment
        primary key,
    vm_id                     int null,
    infrastructure_cluster_id int null,
    constraint infrastructure_cluster_vms_infrastructure_cluster_id_fk
        foreign key (infrastructure_cluster_id) references infrastructure_cluster (id),
    constraint infrastructure_cluster_vms_vms_id_fk
        foreign key (vm_id) references vms (id)
);

create table transport_cluster_vms
(
    id         int auto_increment
        primary key,
    cluster_id int null,
    vm_id      int null,
    constraint cluster_vms_clusters_id_fk
        foreign key (cluster_id) references transport_clusters (id),
    constraint cluster_vms_vms_id_fk
        foreign key (vm_id) references vms (id)
)
    comment 'Виртуальные машины кластеров';

